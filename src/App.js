import React, { useState } from "react";
import styled from "styled-components";

import { BackgroundOptions, LogoOptions } from "./components";
import { EditorArea, Fetch } from "./containers";
import { pickUrls } from "./utils/dataNormalizers";

const App = () => {
  const [background, setBackground] = useState(null);
  return (
    <Wrapper>
      <Editor>
        <Fetch
          url="https://api.unsplash.com/photos?per_page=4"
          dataNormalizer={pickUrls}
        >
          {data => (
            <>
              <BackgroundOptions
                photosUrls={data}
                chooseBackground={setBackground}
                clearBackground={() => setBackground(null)}
              />

              <EditorArea background={background} />
            </>
          )}
        </Fetch>

        <LogoOptions />
      </Editor>
    </Wrapper>
  );
};

export default App;

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
  background-color: white;
`;

const Editor = styled.div`
  display: flex;
  width: 1200px;
  height: 600px;
  border: 1px solid gray;
  border-radius: 10px;
  background-color: black;

  box-shadow: -12px 4px 43px -8px rgba(0, 0, 0, 0.75);
`;
