import React, { memo } from "react";
import styled from "styled-components";
import Button from "../Button";
import H3 from "../H3";

const BackgroundOptions = ({
  photosUrls,
  chooseBackground,
  clearBackground
}) => (
  <Wrapper>
    <H3>Select Background</H3>
    {photosUrls &&
      photosUrls.map(({ thumb, small }) => (
        <Thumb
          key={thumb}
          url={thumb}
          onClick={() => chooseBackground(small)}
        />
      ))}
    <Button text="Delete Background" onClick={clearBackground} />
  </Wrapper>
);

export default memo(BackgroundOptions);

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 30%;
`;

const Thumb = styled.div`
  width: 100px;
  height: 100px;
  background: url(${({ url }) => url}) no-repeat center;
  background-size: cover;
  margin-bottom: 10px;
  border-radius: 5px;
  transition: all 0.2s ease-in-out;
  &:hover {
    transform: scale(1.05);
    box-shadow: -2px 2px 10px 0px rgba(0, 0, 0, 0.48);
  }
`;
