import React from "react";
import styled from "styled-components";

const Button = React.forwardRef(({ text = "click", onClick }, ref) => (
  <SButton ref={ref} onClick={onClick}>
    {text}
  </SButton>
));
export default Button;

const SButton = styled.button`
  background: transparent;
  outline: none;

  font-family: "Comic Sans MS", cursive, sans-serif;
  font-size: 14px;
  font-style: italic;
  color: white;

  border-radius: 5px;
  width: 180px;
  height: 50px;
`;
