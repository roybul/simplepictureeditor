import styled from "styled-components";

const H3 = styled.h3`
  font-family: "Comic Sans MS", cursive, sans-serif;
  font-weight: 100;
  font-style: italic;
  color: white;
`;

export default H3;
