import React from "react";
import styled from "styled-components";
import H3 from "../H3";
import logo1 from "../../assets/1.png";
import logo2 from "../../assets/2.png";
import logo3 from "../../assets/3.png";
import logo4 from "../../assets/4.png";
import logo5 from "../../assets/5.png";

const setDataTransfer = url => e => e.dataTransfer.setData("url", url);

const logo1transfer = setDataTransfer(logo1);
const logo2transfer = setDataTransfer(logo2);
const logo3transfer = setDataTransfer(logo3);
const logo4transfer = setDataTransfer(logo4);
const logo5transfer = setDataTransfer(logo5);

const LogoOptions = () => (
  <Wrapper>
    <H3>Add logo</H3>
    <LogoRow>
      <Logo onDragStart={logo1transfer} draggable url={logo1} alt="logo1" />
      <Logo onDragStart={logo2transfer} draggable url={logo2} alt="logo2" />
      <Logo onDragStart={logo3transfer} draggable url={logo3} alt="logo3" />
      <Logo onDragStart={logo4transfer} draggable url={logo4} alt="logo1" />
      <Logo onDragStart={logo5transfer} draggable url={logo5} alt="logo1" />
    </LogoRow>
  </Wrapper>
);

export default LogoOptions;

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 30%;
`;

const LogoRow = styled.section`
  display: flex;
  justify-content: space-around;
  width: 80%;
`;

const Logo = styled.div`
  width: 40px;
  height: 40px;
  background: url(${({ url }) => url}) no-repeat center;
  background-size: cover;
`;
