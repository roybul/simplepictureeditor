import React from "react";
import { Image } from "react-konva";
import del from "../../assets/deletered.png";

const deleteImg = new window.Image();
deleteImg.src = del;
deleteImg.crossOrigin = "Anonymous";

class ImgKonva extends React.Component {
  state = {
    image: null,
    showDelete: false,
    rect: null,
    deleteImg
  };
  img = React.createRef();

  componentDidMount() {
    const { url } = this.props;
    const image = new window.Image();
    image.crossOrigin = "Anonymous";
    image.src = url;
    image.onload = () => {
      this.setState({ image });
    };
  }

  handleOnClick = e => {
    this.setState(ps => ({
      showDelete: !ps.showDelete,
      rect: ps.rect ? null : this.img.current._lastPos
    }));
  };

  handleDelete = () => {
    const { url, deleteHandler } = this.props;
    deleteHandler(url);
  };

  handleDragStart = () =>
    this.setState({
      showDelete: false,
      rect: null
    });

  render() {
    const { draggable, width, height, deleteHandler } = this.props;
    const { image, showDelete, rect, deleteImg } = this.state;
    return (
      <>
        <Image
          ref={this.img}
          onClick={deleteHandler && this.handleOnClick}
          draggable={draggable}
          onDragStart={this.handleDragStart}
          width={width}
          height={height}
          image={image}
        />
        {showDelete && (
          <Image
            onClick={this.handleDelete}
            height={25}
            width={25}
            {...rect}
            image={deleteImg}
          />
        )}
      </>
    );
  }
}

export default ImgKonva;
