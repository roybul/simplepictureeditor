import React, { PureComponent } from "react";
import { without, includes } from "ramda";
import { Stage, Layer } from "react-konva";
import styled from "styled-components";
import empty_background from "../../assets/empty_background.bmp";
import Img from "./ImgKonva";

class EditorArea extends PureComponent {
  state = { logos: [] };
  canv = React.createRef();

  addLogo = url =>
    !includes(url, this.state.logos) &&
    this.setState(ps => ({ logos: [...ps.logos, url] }));

  deleteLogo = url =>
    this.setState(ps => ({ logos: without([url], ps.logos) }));

  render() {
    const { background } = this.props;
    return (
      <Wrapper>
        <Title>Simple Editor</Title>
        <Droppable
          onDragOver={e => {
            e.preventDefault();
          }}
          onDrop={e => {
            const url = e.dataTransfer.getData("url");
            this.addLogo(url);
          }}
        >
          <Stage width={400} height={400} ref={this.canv}>
            <Layer>
              <Img key={background} url={background || empty_background} />
              {this.state.logos.map(logoUrl => (
                <Img
                  key={logoUrl}
                  draggable
                  width={100}
                  height={100}
                  url={logoUrl}
                  deleteHandler={this.deleteLogo}
                />
              ))}
            </Layer>
          </Stage>
        </Droppable>
        <Download
          download="img.png"
          onClick={e => (e.target.href = this.canv.current.toDataURL())}
          href=""
        >
          Download
        </Download>
      </Wrapper>
    );
  }
}

export default EditorArea;

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  width: 40%;
`;

const Droppable = styled.div`
  position: relative;
  max-width: 400px;
  max-height: 400px;
`;

const Title = styled.h1`
  font-family: "Comic Sans MS", cursive, sans-serif;
  font-weight: 100;
  font-style: italic;
  color: white;
`;

const Download = styled.a`
  color: white;
  transition: all 0.2s ease-in-out;

  &:hover {
    transform: scale(1.2);
  }
`;
