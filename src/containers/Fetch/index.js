import React, { PureComponent } from "react";

class Fetch extends PureComponent {
  state = { data: null, err: null };

  async componentDidMount() {
    const { url, dataNormalizer } = this.props;

    try {
      const data = await fetch(url, {
        headers: {
          "Accept-Version": "v1",
          Authorization:
            "Client-ID a2a3dd5b3ecfd417c372183bb56aca4158cc4aa7780effa6144e4ae4ebc20dcc"
        }
      }).then(resp => resp.json());
      this.setState({ data: dataNormalizer(data) });
    } catch (e) {
      this.setState({ err: e });
    }
  }

  render() {
    const {
      state: { data, err },
      props: { children }
    } = this;
    return !err ? children(data) : <span>Error</span>;
  }
}

export default Fetch;
