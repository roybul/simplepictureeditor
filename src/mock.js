export const photosUrls = [
  {
    small:
      "https://images.unsplash.com/photo-1549261472-fcd48d0b6709?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjU0NDIzfQ",
    thumb:
      "https://images.unsplash.com/photo-1549261472-fcd48d0b6709?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjU0NDIzfQ"
  },
  {
    small:
      "https://images.unsplash.com/photo-1549261472-fcd48d0b6709?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjU0NDIzfQ",
    thumb:
      "https://images.unsplash.com/photo-1549261472-fcd48d0b6709?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjU0NDIzfQ"
  }
];
