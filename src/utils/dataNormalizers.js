import { map, path } from "ramda";

export const pickUrls = arr => map(path(["urls"]), arr);
